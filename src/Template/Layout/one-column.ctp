<!DOCTYPE html>
<html lang="en">
<head>
   <?php
    echo $this->Html->meta(
    'favicon.ico',
    '/favicon.ico',
    ['type' => 'icon']
    )
   ?>
    <?= $this->element('head') ?>
   
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<!-- header -->
<?= $this->cell('Menu'); ?>
<!-- end-header  -->

<!-- content -->
    <div id="main">
        <div class="page-center">
            
            <div class="grid-u">
            <div class="pure-g grid-g">  
                
                <div class="pure-u-5-5 pure-u-md-5-5 grid-u">
                    <div id="nipnotes-sitenews">
                     <?= $this->Flash->render() ?>
                     <?= $this->fetch('content') ?>
                    </div>
                </div>
                

                
            </div><!-- pure-g grid-g -->                       
            </div><!-- grid-u -->
            
        </div><!-- page center -->
                <!--<div class="grid-u" style=" position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 10;
                    background-color: rgba(0,0,0,0.5);">
                        <div class="pure-g grid-g"> 
                            <div style="top:0; left:0; bottom:0;" class="pure-u-3-5 pure-u-md-3-5 grid-u">
                                <div style="bottom:0;" id="nipnotes-sitenews">
                                    <div id="main-division">
                                        <div class="main-bx">
                                            <div class="hd">
                                                Sample Text
                                            </div>
                                            <div>
            asdf

                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                            </div>
                            <div class="pure-u-2-5 pure-u-md-2-5 grid-u">
                                X
                            </div>
                        </div>                      
                 </div>-->
    </div><!--main -->
    <?= $this->Html->script('tucked-menu'); ?>
<!-- end-content --> 
<script>

function doSomething(e){e.target!==e.currentTarget&&(Velocity(els,"slideUp",{duration:300,easing:"ease-in",mobileHA:true}),Velocity(document.getElementById("desc-"+e.target.id),"slideDown",{duration:300,easing:"ease-out",mobileHA:true})),e.stopPropagation()}var els=document.querySelectorAll(".anohana"),theParent=document.querySelector("#dlAccordion");theParent.addEventListener("click",doSomething,!1);

// Velocity(els, 'slideUp', {});
// (function($) {
    
//   var allPanels = $('.accordion > dd').hide();
    
//   $('.accordion > dt > a').click(function() {
//     allPanels.slideUp();
//     $(this).parent().next().slideDown();
//     console.log($(this).parent().next());
//     return false;
//   });

// })($);

// var els = document.querySelectorAll('.anohana');
// var theParent = document.querySelector("#dlAccordion");
// theParent.addEventListener("click", doSomething, false);

// function doSomething(e) {
//     if (e.target !== e.currentTarget) {
//         Velocity(els, 'slideUp', {
//             duration: 500,
//             easing: 'ease-in'
//         });
//         Velocity(document.getElementById('desc-'+e.target.id), 'slideDown', {
//             duration: 500,
//             easing: 'ease-out'
//         });
//     }
//     e.stopPropagation();
// }

//  window.onload = function() {
//  nnSlider.gAccordion('.ano-hana', '#dlAccordion', 500);
//  };
</script>
</body>
</html>