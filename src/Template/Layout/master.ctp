<!DOCTYPE html>
<html lang="en">
<head>
   <?php
    echo $this->Html->meta(
    'favicon.ico',
    '/favicon.ico',
    ['type' => 'icon']
    )
   ?>
    <?= $this->element('head') ?>
   
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<!-- header -->
<?= $this->cell('Menu'); ?>
<!-- end-header  -->

<!-- content -->
    <div id="main">
        <div class="page-center">
            
            <div class="grid-u">
            <div class="pure-g grid-g">  
                <div class="pure-u-1-1 pure-u-md-1-5 grid-u">
                    <?php 
                        if (isset($pages)) {
                            if ($pages->count() > 0) {
                             echo $this->element('sidemenu', ['pages' => $pages]);   
                            } 
                        } 
                    ?>
                        
                </div>
                
                <div class="pure-u-5-5 pure-u-md-4-5 grid-u">
                    <div id="nipnotes-sitenews">
                     <?= $this->Flash->render() ?>
                     <?= $this->fetch('content') ?>
                    </div>
                </div>
                
                
            </div><!-- pure-g grid-g -->                       
            </div><!-- grid-u -->
            
        </div><!-- page center -->
    </div><!--main -->
    <?= $this->Html->script('tucked-menu'); ?>
<!-- end-content --> 
</body>
</html>