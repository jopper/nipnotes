
<!-- parent sidemenu -->
<div id="parentSideMenu">
    <div id="subPageMenu" class="childSideMenu pure-menu side-menu">
        <!--<span  class="pure-menu-heading">

        </span>-->


        <!-- start navi  -->
        <div class="hideToMobile" id="navi">
            <div id="contentbox" class="bx">
                <div id="contentservice">
                    <div class="hd">
                        <div title="下へ移動" class="cbbtn">
                            <a title="下へ移動" class="cbimg" id="cbbtnbtm">
                        </div>
                        <span class="assist">
                            X
                        </span>
                        
                        <span class="sidemenu-greeter">
                            <!--@if (Agent::isTablet())
                                Tablet
                            @else 
                                {{ Agent::platform() . ' ' . Agent::browser() }}                      
                            @endif-->
                            Pages
                        </span>
                    </div>
                </div>
            </div>
            
            
        </div>
        <!-- end navi -->
        
        <ul id="page-option" class="pure-menu-list">
            <?php foreach ($pages as $page): ?>
                <li class="pure-menu-item">
                <?=
                    $this->Html->link(
                        $page->title, 
                        ['controller' => $page->path , 'action' => 'page', '_full' => true, $page->id, $page->alternate_title],
                        ['class' => 'pure-menu-link osaka']
                    );
                ?>
                </li>
            <?php endforeach; ?>
        </ul>

    </div>
    
         <a id="onlineRadioLink" href="http://radiotuna.com/cool-jazz-radio">cool jazz radio</a>
         <script type="text/javascript" src="http://radiotuna.com/OnlineRadioPlayer/EmbedRadio?playerParams=%7B%22styleSelection0%22%3A176%2C%22styleSelection1%22%3A218%2C%22styleSelection2%22%3A253%2C%22textColor%22%3A16777215%2C%22backgroundColor%22%3A6265295%2C%22buttonColor%22%3A14540253%2C%22glowColor%22%3A14540253%2C%22playerSize%22%3A200%2C%22playerType%22%3A%22style%22%7D&width=200&height=244"></script>
</div>
<!-- end parent sidemenu -->