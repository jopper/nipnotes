<?php
    echo $this->Html->meta(
    'favicon.ico',
    '/favicon.ico',
    ['type' => 'icon']
    );
    echo $this->Html->charset();
    echo $this->Html->css(['pure', 'pure-grids-responsive-min', 'themes/simple-blue']);
    echo $this->Html->script(['velocity.min', 'classList.min']);
    //echo $this->Html->script('jquery2.2');
    //echo $this->Html->script('https://code.jquery.com/jquery-2.2.3.min.js');
?>

<title>NipNotes</title>

<?= 
    $this->Html->meta(
        'viewport',
        'width=device-width, initial-scale=1'
    );
?>