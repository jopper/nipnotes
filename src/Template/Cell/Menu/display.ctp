<div class="nipnotes-menu-wrapper ">
    <div class="page-center">    
        <div class="pure-menu nipnotes-menu nipnotes-menu-top">
            <a href="#" class="pure-menu-heading nipnotes-menu-brand">NipNotes</a>
            <a href="#" class="nipnotes-menu-toggle" id="toggle"><s class="bar"></s><s class="bar"></s></a>
        </div>
        <div class="pure-menu pure-menu-horizontal pure-menu-scrollable nipnotes-menu nipnotes-menu-bottom nipnotes-menu-tucked" id="tuckedMenu">
            <div class="nipnotes-menu-screen"></div>
            <ul class="pure-menu-list">
            <?php foreach ($menus as $menu): ?>
                <li class="pure-menu-item menu-item">
                    <?=
                        $this->Html->link(
                            $menu->title, 
                            ['controller' => 'Pages', 'action' => 'view', '_full' => true, $menu->id, $menu->alternate_title],
                            ['class' => 'pure-menu-link menu-title osaka']
                        );
                    ?>
                </li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>