// dependencies: Velocity.js && classList shim

var nnSlider = {};

// // toggle vertical slide 
// nipnotesSlider.verticalSlide = function(element, container, easing, duration) {
//     var el   = document.querySelector(element), // cache click target
//         con  = document.querySelector(container), // cache container target
//         up   = 'slideUp', // store up command 
//         down = 'slideDown'; // store down command
     
//     el.addEventListener('click', function(event) {
        
//         var active = el.classList.contains('active'); // store active state
//         el.classList.toggle('active'); // toggle click target active
        
//         Velocity(con, active ? up : down, {
//             // test and init command 
//             duration: duration, // duration set in function call params
//             easing: easing // easing set in function call params
//         });
        
//         event.preventDefault();
//     }); 
// };

nnSlider.gAccordion = function(eventListeners, container, duration) {
    var els = document.querySelectorAll(eventListeners), // cache click target 
        con = document.querySelector(container), // cache contaier target [parent]
        up  = 'slideUp',
        down = 'slideDown';
        
    con.addEventListener('click', function(e) {
        if (e.target !== e.currentTarget) {
            Velocity(els, 'slideUp', {
                duration: 500,
                easing: 'ease-in'
            });
            Velocity(document.getElementById('desc-'+e.target.id), 'slideDown', {
                duration: 500,
                easing: 'ease-out'
            });
        }
        e.stopPropagation();
    }, false);
};
// codepen example: http://codepen.io/indyplanets/pen/gppPPb/
//window.onload = function() {
 // App.verticalSlide('.drawer-button', '.drawer', 'easeOutCirc', 600);
//};